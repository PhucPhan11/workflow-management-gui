export const environmentKeyCloak = {
    keyCloakUrl : 'http://localhost:8443',
    keyCloakRealm : 'workflow',
    keyCloakClientId : 'work-flow',
    roleAllowed: 'student',
    apiRedirectUri: 'http://localhost:4200',
  };