export enum TaskStatus{
    WAITING = "WAITING",
    TODO = "TODO",
    INPROGRESS = "INPROGRESS",
    TESTING = "TESTING",
    DONE = "DONE"
}