import { Department } from "./department.model";

export class ProjectModel{
    constructor(
        id?: number,
        name?: string,
        description?: string,
        dateStart?: Date,
        department?: Department,
    ){}
}