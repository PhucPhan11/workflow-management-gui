export interface ResponseError {
    title: string;
    status: number;
    detail?: string;
    cause?: string;
    type: string;
}