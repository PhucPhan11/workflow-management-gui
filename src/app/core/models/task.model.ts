import { Department } from "./department.model";
import { LabelTable } from "./label-table.model";
import { Semester } from "./semester.model";
import { TaskPriority } from "./task-priority.enum.model";
import { TaskProject } from "./task-project.model";
import { TaskStatus } from "./task.status.enum.model";

export class TaskModel{ 
    constructor(
        public name: string,
        public description: string,
        public employeeId: number,
        public status: TaskStatus,
        public priority: TaskPriority,
        public file: string,
        public semester: Semester,
        public label: LabelTable,
        public project: TaskProject,
        public department: Department,
        public id?: number,
    ){}
}
