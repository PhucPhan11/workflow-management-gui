import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import * as departmentAction from './department.action'
import { Observable, catchError, map, mergeMap, of, switchMap } from "rxjs";
import { DepartmentService } from "../../service/department.service";

@Injectable()
export class DepartmentEffect{
    constructor(
        private actions$: Actions,
        private departmentService: DepartmentService,
    ){}

    loadDepartment$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(departmentAction.loadDepartment),
            mergeMap(() => this.departmentService.loadDepartment()
                .pipe(
                    map(department =>
                        departmentAction.loadDepartmentSuccess({department})
                    ),
                    catchError(err => of(departmentAction.loadDepartmentSuccess(err)))
                ))
        )
    })

    // createTasks$ = createEffect(() => {
    //     return this.actions$.pipe(
    //         ofType(projectAction.createProject),
    //         switchMap(action => this.createTask(action.createdProject))
    //     )
    // })

    // createTask(createdProject: ProjectModel): Observable<any> {
    //     return this.taskService.createTask(createdProject).pipe(
    //         map(httpResponse => projectAction.createProjectSuccess(httpResponse)),
    //         catchError(err => of(projectAction.createProjectFailed(err)))
    //     )
    // }
}