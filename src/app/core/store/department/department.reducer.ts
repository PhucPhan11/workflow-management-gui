import { Action, createReducer, on } from "@ngrx/store";
import * as departmentAction from './department.action'
import { DepartmentState } from "./department.state";

export const initialState: DepartmentState = {
    department: null,
    error: null,
    saving: false,
    loading: false
}

export const departmentReducer = createReducer(
    initialState,
    on(departmentAction.loadDepartment, (state): DepartmentState => {
        return {
            ...state,
            loading: true,
        }
    }),

    on(departmentAction.loadDepartmentSuccess, (state, { department }): DepartmentState => {
        return {
            ...state,
            department,
            loading: false,
            saving: true,
        }
    }),

    on(departmentAction.loadDepartmentFailed, (state, { error }): DepartmentState => {
        return {
            ...state,
            department: null,
            loading: false,
            saving: false,
            error
        }
    }),

    on(departmentAction.createDepartment, (state): DepartmentState => {
        return {
            ...state,
            loading: true,
            saving: true,
        }
    }),
    on(departmentAction.createDepartmentSuccess, (state): DepartmentState => {
        return {
            ...state,
            loading: false,
            saving: false,
            error: null,
            department: null,
        }
    }),
    on(departmentAction.createDepartmentFailed, (state, {error}): DepartmentState => {
        return {
            ...state,
            loading: false,
            saving: false,
            error,
        }
    }),
)

export function reducer(state: DepartmentState, action: Action) {
    return departmentReducer(state, action);
}