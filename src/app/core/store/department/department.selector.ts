import { createSelector } from '@ngrx/store';
import { AppState } from '../core.state';
import { DepartmentState } from './department.state';


export const selectDepartment = (state: AppState) => state.department;

export const selectDepartments = createSelector(
    selectDepartment, (state: DepartmentState) => state
);

export const selectDepartmentList = createSelector(
    selectDepartment, (state: DepartmentState) => state.department
)

export const selectDepartmentLoading = createSelector(
    selectDepartment, (state: DepartmentState) => state.loading
)

export const selectDepartmentError = createSelector(
    selectDepartment, (state: DepartmentState) => state.error
)
