import { createAction, props } from "@ngrx/store";
import { ResponseError } from "../../models/response-error.model";
import { HttpResponse } from "@angular/common/http";
import { Department } from "../../models/department.model";

const departmentAction = {
    loadDepartment: "[Department] Loading Department",
    loadDepartmentSuccess: "[Department] Load Department Successfully",
    loadDepartmentFailed: "[Department] Load Department Failed",
    createDepartment: "[Department] Create Department",
    createDepartmentSuccessfully: "[Department] Create Department Successfully",
    createDepartmentFailed: "[Department] Create Department Failed"
}

export const createDepartment = createAction(
    departmentAction.createDepartment,
    props<{ createdDepartment: Department }>())
export const createDepartmentSuccess = createAction(
    departmentAction.createDepartmentSuccessfully,
    props<{ httpResponse: HttpResponse<any> }>()
    )
export const createDepartmentFailed = createAction(departmentAction.createDepartmentFailed,
    props<{ error: ResponseError }>()
    )

export const loadDepartment = createAction(departmentAction.loadDepartment)
export const loadDepartmentSuccess = createAction(departmentAction.loadDepartmentSuccess, props<{department: Department[]}>())
export const loadDepartmentFailed = createAction(departmentAction.loadDepartmentFailed, props<{error: ResponseError}>())