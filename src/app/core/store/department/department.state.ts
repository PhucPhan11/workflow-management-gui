import { Department } from "../../models/department.model";
import { ResponseError } from "../../models/response-error.model";

export interface DepartmentState {
    department: Department[];
    loading: boolean;
    saving: boolean;
    error: ResponseError;
}