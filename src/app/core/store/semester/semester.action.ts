import { createAction, props } from "@ngrx/store";
import { ResponseError } from "../../models/response-error.model";
import { HttpResponse } from "@angular/common/http";
import { Semester } from "../../models/semester.model";

const semesterAction = {
    loadSemester: "[Semester] Loading Semester",
    loadSemesterSuccess: "[Semester] Load Semester Successfully",
    loadSemesterFailed: "[Semester] Load Semester Failed",
    createSemester: "[Semester] Create Semester",
    createSemesterSuccessfully: "[Semester] Create Semester Successfully",
    createSemesterFailed: "[Semester] Create Semester Failed"
}

export const createSemester = createAction(
    semesterAction.createSemester,
    props<{ createdSemester: Semester }>())
export const createSemesterSuccess = createAction(
    semesterAction.createSemesterSuccessfully,
    props<{ httpResponse: HttpResponse<any> }>()
    )
export const createSemesterFailed = createAction(semesterAction.createSemesterFailed,
    props<{ error: ResponseError }>()
    )

export const loadSemester = createAction(semesterAction.loadSemester)
export const loadSemesterSuccess = createAction(semesterAction.loadSemesterSuccess, props<{semester: Semester[]}>())
export const loadSemesterFailed = createAction(semesterAction.loadSemesterFailed, props<{error: ResponseError}>())