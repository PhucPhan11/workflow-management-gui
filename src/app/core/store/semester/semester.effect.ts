import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import * as semesterAction from './semester.action'
import { Observable, catchError, map, mergeMap, of, switchMap } from "rxjs";
import { SemesterService } from "../../service/semester.service";

@Injectable()
export class SemesterEffect{
    constructor(
        private actions$: Actions,
        private semesterService: SemesterService,
    ){}

    loadSemester$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(semesterAction.loadSemester),
            mergeMap(() => this.semesterService.loadSemester()
                .pipe(
                    map(semester =>
                        semesterAction.loadSemesterSuccess({semester})
                    ),
                    catchError(err => of(semesterAction.loadSemesterFailed(err)))
                ))
        )
    })

    // createTasks$ = createEffect(() => {
    //     return this.actions$.pipe(
    //         ofType(projectAction.createProject),
    //         switchMap(action => this.createTask(action.createdProject))
    //     )
    // })

    // createTask(createdProject: ProjectModel): Observable<any> {
    //     return this.taskService.createTask(createdProject).pipe(
    //         map(httpResponse => projectAction.createProjectSuccess(httpResponse)),
    //         catchError(err => of(projectAction.createProjectFailed(err)))
    //     )
    // }
}