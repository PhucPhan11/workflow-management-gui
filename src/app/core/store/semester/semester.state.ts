import { Department } from "../../models/department.model";
import { ResponseError } from "../../models/response-error.model";
import { Semester } from "../../models/semester.model";

export interface SemesterState {
    semester: Semester[];
    loading: boolean;
    saving: boolean;
    error: ResponseError;
}