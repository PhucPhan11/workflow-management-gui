import { createSelector } from '@ngrx/store';
import { AppState } from '../core.state';
import { SemesterState } from './semester.state';


export const selectSemester = (state: AppState) => state.semester;

export const selectSemesters = createSelector(
    selectSemester, (state: SemesterState) => state
);

export const selectSemesterList = createSelector(
    selectSemester, (state: SemesterState) => state.semester
)

export const selectSemesterLoading = createSelector(
    selectSemester, (state: SemesterState) => state.loading
)

export const selectSemesterError = createSelector(
    selectSemester, (state: SemesterState) => state.error
)
