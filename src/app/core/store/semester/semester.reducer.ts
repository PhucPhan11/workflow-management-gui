import { Action, createReducer, on } from "@ngrx/store";
import * as semesterAction from './semester.action'
import { SemesterState } from "./semester.state";

export const initialState: SemesterState = {
    semester: null,
    error: null,
    saving: false,
    loading: false
}

export const semesterReducer = createReducer(
    initialState,
    on(semesterAction.loadSemester, (state): SemesterState => {
        return {
            ...state,
            loading: true,
        }
    }),

    on(semesterAction.loadSemesterSuccess, (state, { semester }): SemesterState => {
        return {
            ...state,
            semester,
            loading: false,
            saving: true,
        }
    }),

    on(semesterAction.loadSemesterFailed, (state, { error }): SemesterState => {
        return {
            ...state,
            semester: null,
            loading: false,
            saving: false,
            error
        }
    }),

    on(semesterAction.createSemester, (state): SemesterState => {
        return {
            ...state,
            loading: true,
            saving: true,
        }
    }),
    on(semesterAction.createSemesterSuccess, (state): SemesterState => {
        return {
            ...state,
            loading: false,
            saving: false,
            error: null,
            semester: null,
        }
    }),
    on(semesterAction.createSemesterFailed, (state, {error}): SemesterState => {
        return {
            ...state,
            loading: false,
            saving: false,
            error,
        }
    }),
)

export function reducer(state: SemesterState, action: Action) {
    return semesterReducer(state, action);
}