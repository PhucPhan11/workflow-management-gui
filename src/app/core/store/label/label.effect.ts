import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import * as labelAction from './label.action'
import { Observable, catchError, map, mergeMap, of, switchMap } from "rxjs";
import { DepartmentService } from "../../service/department.service";
import { LabelService } from "../../service/label.service";

@Injectable()
export class LabelEffect{
    constructor(
        private actions$: Actions,
        private labelService: LabelService,
    ){}

    loadLabel$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(labelAction.loadLabel),
            mergeMap(() => this.labelService.loadLabel()
                .pipe(
                    map(label =>
                        labelAction.loadLabelSuccess({label})
                    ),
                    catchError(err => of(labelAction.loadLabelFailed(err)))
                ))
        )
    })

    createLabelSucessfully$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(labelAction.createLabelSuccess),
            mergeMap(() => this.labelService.loadLabel()
                .pipe(
                    map(label =>
                        labelAction.loadLabelSuccess({label})
                    ),
                    catchError(err => of(labelAction.loadLabelFailed(err)))
                ))
        )
    })


    deleteLabelSucessfully$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(labelAction.deleteLabelSuccess),
            mergeMap(() => this.labelService.loadLabel()
                .pipe(
                    map(label =>
                        labelAction.loadLabelSuccess({label})
                    ),
                    catchError(err => of(labelAction.loadLabelFailed(err)))
                ))
        )
    })

    createLabel$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(labelAction.createLabel),
            switchMap(action => this.createLabel(action.createdLabel))
        )
    })

    createLabel(createdLabel: any): Observable<any> {
        return this.labelService.createLabel(createdLabel).pipe(
            map(httpResponse => labelAction.createLabelSuccess(httpResponse)),
            catchError(err => of(labelAction.createLabelFailed(err)))
        )
    }

    deleteLabel$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(labelAction.deleteLabel),
            switchMap(action => this.deleteLabel(action.id))
        )
    })

    deleteLabel(id:number): Observable<any> {
        return this.labelService.delete(id).pipe(
            map(httpResponse => labelAction.deleteLabelSuccess(httpResponse)),
            catchError(err => of(labelAction.deleteLabelFailed(err)))
        )
    }
}