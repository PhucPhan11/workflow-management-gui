import { createSelector } from '@ngrx/store';
import { AppState } from '../core.state';
import { LabelState } from './label.state';


export const selectLabel = (state: AppState) => state.label;

export const selectLabels = createSelector(
    selectLabel, (state: LabelState) => state
);

export const selectLabelList = createSelector(
    selectLabel, (state: LabelState) => state.labels
)

export const selectLabelLoading = createSelector(
    selectLabel, (state: LabelState) => state.loading
)

export const selectLabelError = createSelector(
    selectLabel, (state: LabelState) => state.error
)
