import { LabelTable } from "../../models/label-table.model";
import { ResponseError } from "../../models/response-error.model";

export interface LabelState {
    labels: LabelTable[];
    loading: boolean;
    saving: boolean;
    error: ResponseError;
}