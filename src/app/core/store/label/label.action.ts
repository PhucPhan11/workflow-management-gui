import { createAction, props } from "@ngrx/store";
import { ResponseError } from "../../models/response-error.model";
import { HttpResponse } from "@angular/common/http";
import { LabelTable } from "../../models/label-table.model";

const labelAction = {
    loadLabel: "[Label] Loading Label",
    loadLabelSuccess: "[Label] Load Label Successfully",
    loadLabelFailed: "[Label] Load Label Failed",
    createLabel: "[Label] Create Label",
    createLabelSuccessfully: "[Label] Create Label Successfully",
    createLabelFailed: "[Label] Create Label Failed",
    deleteLabel: "[Label] deleteing Label",
    deleteLabelSuccess: "[Label] delete Label Successfully",
    deleteLabelFailed: "[Label] delete Label Failed",
}

export const createLabel = createAction(
    labelAction.createLabel,
    props<{ createdLabel: LabelTable }>())
export const createLabelSuccess = createAction(
    labelAction.createLabelSuccessfully,
    props<{ httpResponse: HttpResponse<any> }>()
    )
export const createLabelFailed = createAction(labelAction.createLabelFailed,
    props<{ error: ResponseError }>()
    )

export const loadLabel = createAction(labelAction.loadLabel)
export const loadLabelSuccess = createAction(labelAction.loadLabelSuccess, props<{label: LabelTable[]}>())
export const loadLabelFailed = createAction(labelAction.loadLabelFailed, props<{error: ResponseError}>())

export const deleteLabel = createAction(labelAction.deleteLabel,
    props<{id: number}>()
    )
export const deleteLabelSuccess = createAction(labelAction.deleteLabelSuccess,
    props<{ httpResponse: HttpResponse<any> }>()
    )
export const deleteLabelFailed = createAction(labelAction.deleteLabelFailed, props<{error: ResponseError}>())