import { Action, createReducer, on } from "@ngrx/store";
import * as labelAction from './label.action'
import { LabelState } from "./label.state";

export const initialState: LabelState = {
    labels: null,
    error: null,
    saving: false,
    loading: false
}

export const labelReducer = createReducer(
    initialState,
    on(labelAction.loadLabel, (state): LabelState => {
        return {
            ...state,
            loading: true,
        }
    }),

    on(labelAction.loadLabelSuccess, (state, { label }): LabelState => {
        return {
            ...state,
            labels: label,
            loading: false,
            saving: true,
        }
    }),

    on(labelAction.loadLabelFailed, (state, { error }): LabelState => {
        return {
            ...state,
            labels: null,
            loading: false,
            saving: false,
            error
        }
    }),

    on(labelAction.createLabel, (state): LabelState => {
        return {
            ...state,
            loading: true,
            saving: true,
        }
    }),
    on(labelAction.createLabelSuccess, (state): LabelState => {
        return {
            ...state,
            loading: false,
            saving: false,
            error: null,
            labels: null,
        }
    }),
    on(labelAction.createLabelFailed, (state, {error}): LabelState => {
        return {
            ...state,
            loading: false,
            saving: false,
            error,
        }
    }),
)

export function reducer(state: LabelState, action: Action) {
    return labelReducer(state, action);
}