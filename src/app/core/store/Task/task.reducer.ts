import { Action, createReducer, on } from "@ngrx/store";
import { TaskState } from "./task.state";
import * as taskAction from './task.action'

export const initialState: TaskState = {
    task: null,
    error: null,
    saving: false,
    loading: false
}

export const taskReducer = createReducer(
    initialState,
    on(taskAction.loadTask, (state): TaskState => {
        return {
            ...state,
            loading: true,
        }
    }),

    on(taskAction.loadTaskSuccess, (state, { task }): TaskState => {
        return {
            ...state,
            task,
            loading: false,
            saving: true,
        }
    }),

    on(taskAction.loadTaskFailed, (state, { error }): TaskState => {
        return {
            ...state,
            task: null,
            loading: false,
            saving: false,
            error
        }
    }),

    on(taskAction.createTask, (state): TaskState => {
        return {
            ...state,
            loading: true,
            saving: true,
        }
    }),
    on(taskAction.createTaskSuccess, (state): TaskState => {
        return {
            ...state,
            loading: false,
            saving: false,
            error: null,
            task: null,
        }
    }),
    on(taskAction.createTaskFailed, (state, {error}): TaskState => {
        return {
            ...state,
            loading: false,
            saving: false,
            error,
        }
    }),
)

export function reducer(state: TaskState, action: Action) {
    return taskReducer(state, action);
}