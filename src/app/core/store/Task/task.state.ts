import { ResponseError } from "../../models/response-error.model";
import { TaskModel } from "../../models/task.model";

export interface TaskState {
    task: TaskModel[];
    loading: boolean;
    saving: boolean;
    error: ResponseError;
}