import { createAction, props } from "@ngrx/store";
import { TaskModel } from "../../models/task.model";
import { ResponseError } from "../../models/response-error.model";
import { HttpResponse } from "@angular/common/http";

const taskAction = {
    loadTask: "[TASKS] Loading Task",
    loadTaskSuccess: "[TASKS] Load Task Successfully",
    loadTaskFailed: "[TASKS] Load Task Failed",
    createTask: "[TASKS] Create Task",
    createTaskSuccessfully: "[TASKS] Create Task Successfully",
    createTaskFailed: "[TASKS] Create Task Failed"
}

export const createTask = createAction(
    taskAction.createTask,
    props<{ createdTask: TaskModel }>())
export const createTaskSuccess = createAction(
    taskAction.createTaskSuccessfully,
    props<{ httpResponse: HttpResponse<any> }>()
    )
export const createTaskFailed = createAction(taskAction.createTaskFailed,
    props<{ error: ResponseError }>()
    )

export const loadTask = createAction(taskAction.loadTask)
export const loadTaskSuccess = createAction(taskAction.loadTaskSuccess, props<{task: TaskModel[]}>())
export const loadTaskFailed = createAction(taskAction.loadTaskFailed, props<{error: ResponseError}>())
// export const loadTask = createAction(taskAction.load,props<{id: number}>())