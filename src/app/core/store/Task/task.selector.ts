import { createSelector } from '@ngrx/store';
import { AppState } from '../core.state';
import { TaskState } from './task.state';


export const selectTask = (state: AppState) => state.tasks;

export const selectTasks = createSelector(
    selectTask, (state: TaskState) => state.task
);