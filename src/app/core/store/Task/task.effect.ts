import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import * as taskAction from './task.action'
import { Observable, catchError, map, mergeMap, of, switchMap } from "rxjs";
import { TaskService } from "../../service/task.service";
import { TaskModel } from "../../models/task.model";
@Injectable()
export class taskEffect{
    constructor(
        private actions$: Actions,
        private taskService: TaskService,
    ){}

    loadTasks$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(taskAction.loadTask),
            mergeMap(() => this.taskService.loadTask()
                .pipe(
                    map(task => 
                        taskAction.loadTaskSuccess({task})
                    ),
                    catchError(err => of(taskAction.loadTaskFailed(err)))
                ))
        )
    })

    createSucess$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(taskAction.createTaskSuccess),
            mergeMap(() => this.taskService.loadTask()
                .pipe(
                    map(task => 
                        taskAction.loadTaskSuccess({task})
                    ),
                    catchError(err => of(taskAction.loadTaskFailed(err)))
                ))
        )
    })

    createTasks$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(taskAction.createTask),
            switchMap(action => this.createTask(action.createdTask))
        )
    })

    createTask(createdTask: TaskModel): Observable<any> {
        return this.taskService.createTask(createdTask).pipe(
            map(httpResponse => taskAction.createTaskSuccess(httpResponse)),
            catchError(err => of(taskAction.createTaskFailed(err)))
        )
    }
}