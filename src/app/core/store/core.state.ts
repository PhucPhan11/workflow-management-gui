import { ResponseError } from "../models/response-error.model";
import { TaskState } from "./Task/task.state";
import { DepartmentState } from "./department/department.state";
import { LabelState } from "./label/label.state";
import { ProjectState } from "./project/project.state";
import { SemesterState } from "./semester/semester.state";

export interface AppState {
    tasks: TaskState,
    project: ProjectState,
    department: DepartmentState,
    semester: SemesterState,
    label: LabelState
}