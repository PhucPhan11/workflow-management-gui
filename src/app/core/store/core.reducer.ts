import { ActionReducerMap } from "@ngrx/store";
import { AppState } from "./core.state";
import * as TaskReducer from "./Task/task.reducer";
import * as projectReducer from "./project/project.reducer";
import * as departmentReducer from "./department/department.reducer";
import * as labelReducer from "./label/label.reducer";
import * as semesterReducer from "./semester/semester.reducer";

export const reducers: ActionReducerMap<AppState, any> = {
    tasks: TaskReducer.reducer,
    project: projectReducer.reducer,
    department: departmentReducer.reducer,
    label: labelReducer.reducer,
    semester: semesterReducer.reducer,
}