import { ProjectModel } from "../../models/project.model";
import { ResponseError } from "../../models/response-error.model";

export interface ProjectState {
    projects: ProjectModel[];
    loading: boolean;
    saving: boolean;
    error: ResponseError;
}