import { Injectable } from "@angular/core";
import { Actions, createEffect, ofType } from "@ngrx/effects";
import * as projectAction from './project.action'
import { Observable, catchError, map, mergeMap, of, switchMap } from "rxjs";
import { ProjectService } from "../../service/project.service";
import { ProjectModel } from "../../models/project.model";
import { CreatedProjectModel } from "../../models/created-project.model";

@Injectable()
export class projectEffect{
    constructor(
        private actions$: Actions,
        private projectService: ProjectService,
    ){}

    loadProjects$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(projectAction.loadProject),
            mergeMap(() => this.projectService.loadProject()
                .pipe(
                    map(projects =>
                        projectAction.loadProjectSuccess({projects})
                    ),
                    catchError(err => of(projectAction.loadProjectFailed(err)))
                ))
        )
    })

    createProjectSuccessfully$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(projectAction.createProjectSuccess),
            mergeMap(() => this.projectService.loadProject()
                .pipe(
                    map(projects =>
                        projectAction.loadProjectSuccess({projects})
                    ),
                    catchError(err => of(projectAction.loadProjectFailed(err)))
                ))
        )
    })

    deleteProjectSuccessfully$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(projectAction.deleteProjectSuccess),
            mergeMap(() => this.projectService.loadProject()
                .pipe(
                    map(projects =>
                        projectAction.loadProjectSuccess({projects})
                    ),
                    catchError(err => of(projectAction.loadProjectFailed(err)))
                ))
        )
    })


    createProject$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(projectAction.createProject),
            switchMap(action => this.createProject(action.createdProject))
        )
    })

    createProject(createdProject: CreatedProjectModel): Observable<any> {
        return this.projectService.createProject(createdProject).pipe(
            map(httpResponse => projectAction.createProjectSuccess(httpResponse)),
            catchError(err => of(projectAction.createProjectFailed(err)))
        )
    }

    deleteProject$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(projectAction.deleteProject),
            switchMap(action => this.deleteProject(action.id))
        )
    })

    deleteProject(id:number): Observable<any> {
        return this.projectService.deleteProject(id).pipe(
            map(httpResponse => projectAction.deleteProjectSuccess(httpResponse)),
            catchError(err => of(projectAction.deleteProjectFailed(err)))
        )
    }
}