import { Action, createReducer, on } from "@ngrx/store";
import * as projectAction from './project.action'
import { ProjectState } from "./project.state";

export const initialState: ProjectState = {
    projects: null,
    error: null,
    saving: false,
    loading: false
}

export const projectReducer = createReducer(
    initialState,
    on(projectAction.loadProject, (state): ProjectState => {
        return {
            ...state,
            loading: true,
        }
    }),

    on(projectAction.loadProjectSuccess, (state, { projects }): ProjectState => {
        return {
            ...state,
            projects,
            loading: false,
            saving: true,
        }
    }),

    on(projectAction.loadProjectFailed, (state, { error }): ProjectState => {
        return {
            ...state,
            projects: null,
            loading: false,
            saving: false,
            error
        }
    }),

    on(projectAction.createProject, (state): ProjectState => {
        return {
            ...state,
            loading: true,
            saving: true,
        }
    }),
    on(projectAction.createProjectSuccess, (state): ProjectState => {
        return {
            ...state,
            loading: false,
            saving: false,
            error: null,
            projects: null,
        }
    }),
    on(projectAction.createProjectFailed, (state, {error}): ProjectState => {
        return {
            ...state,
            loading: false,
            saving: false,
            error,
        }
    }),
)

export function reducer(state: ProjectState, action: Action) {
    return projectReducer(state, action);
}