import { createSelector } from '@ngrx/store';
import { AppState } from '../core.state';
import { ProjectState } from './project.state';
import { ProjectModel } from '../../models/project.model';


export const selectProject = (state: AppState) => state.project;

export const selectProjects = createSelector(
    selectProject, (state: ProjectState) => state
);

export const selectProjectList = createSelector(
    selectProject, (state: ProjectState) => state.projects
)

export const selectProjectLoading = createSelector(
    selectProject, (state: ProjectState) => state.loading
)

export const selectProjectError = createSelector(
    selectProject, (state: ProjectState) => state.error
)
