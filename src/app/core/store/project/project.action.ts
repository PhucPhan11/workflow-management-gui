import { createAction, props } from "@ngrx/store";
import { ResponseError } from "../../models/response-error.model";
import { HttpResponse } from "@angular/common/http";
import { ProjectModel } from "../../models/project.model";
import { CreatedProjectModel } from "../../models/created-project.model";

const projectAction = {
    loadProject: "[PROJECT] Loading Project",
    loadProjectSuccess: "[PROJECT] Load Project Successfully",
    loadProjectFailed: "[PROJECT] Load Project Failed",
    createProject: "[PROJECT] Create Project",
    createProjectSuccessfully: "[PROJECT] Create Project Successfully",
    createProjectFailed: "[PROJECT] Create Project Failed",
    deleteProject: "[PROJECT] Delete Project",
    deleteProjectSuccessfully: "[PROJECT] Delete Project Successfully",
    deleteProjectFailed: "[PROJECT] Delete Project Failed",
}

export const createProject = createAction(
    projectAction.createProject,
    props<{ createdProject: CreatedProjectModel }>())
export const createProjectSuccess = createAction(
    projectAction.createProjectSuccessfully,
    props<{ httpResponse: HttpResponse<any> }>()
    )
export const createProjectFailed = createAction(projectAction.createProjectFailed,
    props<{ error: ResponseError }>()
    )

export const loadProject = createAction(projectAction.loadProject)
export const loadProjectSuccess = createAction(projectAction.loadProjectSuccess, props<{projects: ProjectModel[]}>())
export const loadProjectFailed = createAction(projectAction.loadProjectFailed, props<{error: ResponseError}>())

export const deleteProject = createAction(projectAction.deleteProject,
    props<{id: number}>()
    )
export const deleteProjectSuccess = createAction(projectAction.deleteProjectSuccessfully,
    props<{ httpResponse: HttpResponse<any> }>()
    )
export const deleteProjectFailed = createAction(projectAction.deleteProjectFailed, props<{error: ResponseError}>())