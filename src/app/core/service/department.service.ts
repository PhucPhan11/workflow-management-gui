import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DepartmentService {
  constructor(
    private http$: HttpClient
  ) { }

  loadDepartment(): Observable<any> {
    const url = 'http://localhost:8080/workflowmanament/rest/department';
    return this.http$.get(url)
  }
}
