import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SemesterService {

  constructor(private http$: HttpClient) { }
  loadSemester(): Observable<any> {
    const url = 'http://localhost:8080/workflowmanament/rest/semester';
    return this.http$.get(url)
  }
}
