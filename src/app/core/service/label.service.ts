import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class LabelService {
  url = 'http://localhost:8080/workflowmanament/rest/label';
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  }
  constructor(
    private http$: HttpClient
  ) { }

  loadLabel(): Observable<any> {
    return this.http$.get(this.url)
  }

  createLabel(createLabel: any): Observable<any> {
    return this.http$.post(this.url, createLabel, this.httpOptions)
  }
  
  delete(id): Observable<any> {
    return this.http$.delete(`${this.url}/${id}`)
  }
}
