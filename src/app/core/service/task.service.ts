import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { TaskModel } from '../models/task.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  url = "http://localhost:8080/workflowmanament/rest/task"
  constructor(private http$: HttpClient) {}

  loadTask(): Observable<any> { 
    return this.http$.get(this.url)
  }

  createTask(task: TaskModel): Observable<any> { 
    return this.http$.post(this.url, task)
  }
}
