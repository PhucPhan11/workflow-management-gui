import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { CreatedProjectModel } from '../models/created-project.model';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {
  url = 'http://localhost:8080/workflowmanament/rest/project';
  httpOptions = {
    headers: new HttpHeaders({'Content-Type': 'application/json'})
  }
  constructor(
    private http$: HttpClient
  ) { }

  loadProject(): Observable<any> {
    return this.http$.get(this.url)
  }

  createProject(createProject: CreatedProjectModel): Observable<any> {
    return this.http$.post(this.url,createProject, this.httpOptions)
  }
  deleteProject(id: number): Observable<any> {
    return this.http$.delete(`${this.url}/${id}`)
  }
}
