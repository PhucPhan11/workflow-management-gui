import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { reducers } from './store/core.reducer';
import { EffectsModule } from '@ngrx/effects';
import { taskEffect } from './store/Task/task.effect';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { projectEffect } from './store/project/project.effect';
import { DepartmentEffect } from './store/department/department.effect';
import { SemesterEffect } from './store/semester/semester.effect';
import { LabelEffect } from './store/label/label.effect';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    StoreModule.forRoot(reducers),
    EffectsModule.forRoot([
      taskEffect,
      projectEffect,
      DepartmentEffect,
      SemesterEffect,
      LabelEffect
    ]),
    StoreDevtoolsModule.instrument({maxAge: 25})
  ],
})
export class CoreModule {}
