import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth/auth.guard';
import { DeniedComponent } from './denied/denied.component';
import { environmentKeyCloak } from 'src/environments/environment';
import { HomeComponent } from './features/home/home.component';
import { ProjectComponent } from './features/project/project.component';
import { DepartmentComponent } from './features/department/department.component';
import { TaskComponent } from './features/home/task/task.component';
import { LabelComponent } from './features/label/label.component';

export const routes: Routes = [
  { path: '', redirectTo:"/task", pathMatch: "full"},
  { path: 'task',  canActivate: [AuthGuard],component: TaskComponent, data: { roles: [environmentKeyCloak.roleAllowed]}},
  { path: 'project', canActivate: [AuthGuard], component: ProjectComponent, data: { roles: [environmentKeyCloak.roleAllowed]}},
  { path: 'department', canActivate: [AuthGuard], component: DepartmentComponent, data: { roles: [environmentKeyCloak.roleAllowed]} },
  { path: 'label', canActivate: [AuthGuard], component: LabelComponent, data: { roles: [environmentKeyCloak.roleAllowed]} },
  { path: 'denied', component: DeniedComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }