import { Injectable } from "@angular/core";
import { ActivatedRouteSnapshot, Router, RouterStateSnapshot } from "@angular/router";
import { KeycloakAuthGuard, KeycloakService } from "keycloak-angular";

@Injectable()
export class AuthGuard extends KeycloakAuthGuard {

    granted: boolean = false;

    constructor(protected override router: Router, protected keycloak: KeycloakService) {
        super(router, keycloak);
    }
   
    isAccessAllowed(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):  Promise<boolean>  {
        return new Promise ((resolve, reject) => {
            if(!this.authenticated){
                this.keycloak.login();
                return;
            }

            const requiredRoles  = route.data['roles'];

            if (!requiredRoles || requiredRoles.length === 0) {
                this.granted = true;    
            } else {
                for (const requiredRole of requiredRoles) {
                    if (this.roles.indexOf(requiredRole) > -1) {
                        this.granted = true;
                        break;
                    }
                }
            }

            if (this.granted === false) {   
                this.router.navigate(['denied']);
            }
            resolve(this.granted);
        });
        
    }

    onLogin(){
        return this.granted;
    }
}