import { KeycloakService } from 'keycloak-angular';
import { environmentKeyCloak } from '../../environments/environment';

export function initializeKeycloak(keycloak: KeycloakService): () => Promise<boolean> {
    let urlKeyCloak = environmentKeyCloak.keyCloakUrl
    let realmKeyCloak = environmentKeyCloak.keyCloakRealm
    let clientIdKeyCloak = environmentKeyCloak.keyCloakClientId

    return () =>
        keycloak.init({
            config: {
                url: urlKeyCloak,
                realm: realmKeyCloak,
                clientId: clientIdKeyCloak,
            },
            initOptions: {
                onLoad: 'login-required',
                flow: 'standard',
                checkLoginIframe: false,
            },  
        })
}

