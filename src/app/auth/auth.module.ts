import { APP_INITIALIZER, NgModule } from "@angular/core";
import { KeycloakAngularModule, KeycloakService } from "keycloak-angular";
import { AuthGuard } from "./auth.guard";
import { AuthService } from "./auth.service";
import { initializeKeycloak } from "./keycloak-initializer";

@NgModule({
    declarations: [],
    imports: [KeycloakAngularModule],
    providers: [
        {
            provide: APP_INITIALIZER,
            useFactory: initializeKeycloak,
            multi: true,
            deps: [KeycloakService]
        },
        AuthGuard,
        AuthService
    ]
})
export class AuthModule {}