import { Injectable } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { environmentKeyCloak } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  isAdmin = false;

  constructor(private keycloakService: KeycloakService) { }

  getRoles(): string[]{
    return this.keycloakService.getUserRoles(); 
  }

  isAuthenticated() {
    const promise = new Promise(
      (resolve) => {
        setTimeout(() => {
          resolve(this.isAdminRole);
        }, 800);
      }
    );
    return promise;
  }

  isAdminRole() {
    let roleAllowed = environmentKeyCloak.roleAllowed;
    return this.keycloakService.isUserInRole(roleAllowed);
  }
}
