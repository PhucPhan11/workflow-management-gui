import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';

import { AuthGuard } from '../auth/auth.guard';
import { environmentKeyCloak } from 'src/environments/environment';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  lastName: any;

  firstName: any;

  fullName: any;

  userName: any;

  granted: boolean = false;

  isActive: boolean = true;

  constructor(
    private keycloack: KeycloakService,
    private auth: AuthGuard) { }

  ngOnInit(): void {
    this.keycloack.loadUserProfile().then(
      value => {
        this.granted = this.auth.granted;
        this.firstName = value.firstName;
        this.lastName = value.lastName;
        this.userName = value.username;
        if (!!this.firstName && !!this.lastName) {
          this.fullName = [this.firstName + ' ' + this.lastName];
        } else {
          this.fullName = this.userName;
        }
      }
    );
  }

  onLogOut() {
    this.keycloack.clearToken();
    this.keycloack.logout(environmentKeyCloak.apiRedirectUri);
  }
}
