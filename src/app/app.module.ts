import { NgModule } from '@angular/core';
import { Store } from '@ngrx/store';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTableModule } from '@angular/material/table';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import {MatDividerModule} from '@angular/material/divider';
import {MatDialogContent, MatDialogModule} from '@angular/material/dialog';
import {MatInputModule} from '@angular/material/input';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatListModule} from '@angular/material/list'
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatDatepickerModule} from '@angular/material/datepicker';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { ActivatedRoute, RouterModule } from '@angular/router';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatAutocompleteModule} from '@angular/material/autocomplete';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { AuthModule } from './auth/auth.module';
import { HomeComponent } from './features/home/home.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { TaskListComponent } from './features/home/task/task-list/task-list.component';
import { TaskComponent } from './features/home/task/task.component';
import { CreateTaskComponent } from './features/home/task/create-task/create-task.component';
import { DepartmentComponent } from './features/department/department.component';
import { ProjectComponent } from './features/project/project.component';
import { AppRoutingModule } from './app-routing.module';
import { CreateProjectComponent } from './features/project/create-project/create-project.component';
import { ConfirmationPopupComponent } from './features/confirmation-popup/confirmation-popup.component';
import { LabelComponent } from './features/label/label.component';
import { CreateLabelComponent } from './features/label/create-label/create-label.component';
import { CreateDepartmentComponent } from './features/department/create-department/create-department.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    WelcomeComponent,
    HomeComponent,
    TaskListComponent,
    TaskComponent,
    CreateTaskComponent,
    DepartmentComponent,
    ProjectComponent,
    CreateProjectComponent,
    ConfirmationPopupComponent,
    LabelComponent,
    CreateLabelComponent,
    CreateDepartmentComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    CoreModule,
    BrowserAnimationsModule,
    AuthModule,
    MatButtonModule,
    MatTableModule,
    MatToolbarModule,
    MatIconModule,
    MatDividerModule,
    MatDialogModule,
    MatFormFieldModule,
    HttpClientModule,
    MatInputModule,
    MatSidenavModule,
    RouterModule,
    MatListModule,
    MatProgressSpinnerModule,
    MatPaginatorModule, 
    MatSelectModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatExpansionModule,
    MatCheckboxModule,
    MatChipsModule,
    MatAutocompleteModule
    
  ],
  providers: [
    Store,
    AuthModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
