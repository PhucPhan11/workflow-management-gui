import { Component, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-denied',
  templateUrl: './denied.component.html',
  styleUrls: ['./denied.component.scss']
})
export class DeniedComponent implements OnInit {

  @Output('activedDenied') activedDenied: boolean = true;

  constructor() { }

  ngOnInit(): void {
  }

}
