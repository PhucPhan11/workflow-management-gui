import { Semester } from "../core/models/semester.model";
import { TaskPriority } from "../core/models/task-priority.enum.model";
import { TaskModel } from "../core/models/task.model";
import { TaskStatus } from "../core/models/task.status.enum.model";

// export const mockTasks: TaskModel[] = [
//     {
//         name: "first task",
//         description: "test task description",
//         employeeId: 1809169,
//         status: TaskStatus.TODO,
//         priority: TaskPriority.LOWEST,
//         file: "Test file",
//         semester: {
//             id: 1,
//             name: "semester name",
//             description: "semester desc",
//             dateStart: Date(),
//             dateEnd: Date(),
//         },
//         label: {
//             id: 1,
//             name: "label name",
//             description: "description label"
//         },
//         project: {
//             id:1,
//             name: "project name",
//             description: "project desc",
//         },
//         department: {
//             id: 1,
//             name: "department name",
//             description: "department label"
//         },
//     },
//     {
//         name: "second task",
//         description: "test second task description",
//         employeeId: 1809169,
//         status: TaskStatus.INPROGRESS,
//         priority: TaskPriority.HIGHEST,
//         file: "Test file",
//         semester: {
//             id: 2,
//             name: "semester name",
//             description: "semester desc",
//             dateStart: Date(),
//             dateEnd: Date(),
//         },
//         label: {
//             id: 2,
//             name: "label name",
//             description: "description label"
//         },
//         project: {
//             id:2,
//             name: "project name",
//             description: "project desc",
//         },
//         department: {
//             id: 2,
//             name: "department name",
//             description: "department label"
//         },
//     },
// ]