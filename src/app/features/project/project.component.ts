import { LiveAnnouncer } from '@angular/cdk/a11y';
import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Store } from '@ngrx/store';
import { Observable, delay } from 'rxjs';
import { selectProjectList, selectProjectLoading } from 'src/app/core/store/project/project.selector';
import * as projectAction from './../../core/store/project/project.action'
import { ProjectModel } from 'src/app/core/models/project.model';
import { MatDialog } from '@angular/material/dialog';
import { CreateProjectComponent } from './create-project/create-project.component';
import { FormBuilder, UntypedFormBuilder, Validators } from '@angular/forms';
import { ConfirmationPopupComponent } from '../confirmation-popup/confirmation-popup.component';


@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  
  projects$: Observable<ProjectModel[]> = this.store.select(selectProjectList)
  projectsLoading$: Observable<boolean> = this.store.select(selectProjectLoading)
  displayedColumns: string[] = ['No','name', 'description', 'dateStart', 'department','action'];
  dataSource = new MatTableDataSource<ProjectModel>();

  constructor(
    private store: Store,
    private dialog: MatDialog,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
  
  }

  ngAfterViewInit() {
    this.store.dispatch(projectAction.loadProject())
    this.projects$
    .subscribe((data:ProjectModel[]) => {
      this.dataSource.data = data
      // this.dialog.closeAll();
    })
    this.dataSource.paginator = this.paginator
  }

  openDialog() {
    this.dialog.open(CreateProjectComponent);
  }

  handleDel(id: number) {
    this.dialog.open(ConfirmationPopupComponent).afterClosed().subscribe(() => {
      this.store.dispatch(projectAction.deleteProject({id}))
    })

    
  }

}
