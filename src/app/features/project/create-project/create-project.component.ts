import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, UntypedFormControl, Validators } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { Store } from '@ngrx/store';
import moment from 'moment';
import { BehaviorSubject, Subject } from 'rxjs';
import { CreatedProjectModel } from 'src/app/core/models/created-project.model';
import { ProjectModel } from 'src/app/core/models/project.model';
import { loadDepartment } from 'src/app/core/store/department/department.action';
import { selectDepartment, selectDepartmentList, selectDepartments } from 'src/app/core/store/department/department.selector';
import { createProject } from 'src/app/core/store/project/project.action';

@Component({
  selector: 'app-create-project',
  templateUrl: './create-project.component.html',
  styleUrls: ['./create-project.component.scss']
})
export class CreateProjectComponent implements OnInit {
  projectForm: FormGroup;
  selectDepartMents$ = this.store.select(selectDepartmentList)
  departmentId: number;
  constructor(
    private fb: FormBuilder,
    private store: Store
  ){}

  ngOnInit() {
    this.store.dispatch(loadDepartment())
    this.projectForm = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      dateStart: ['', Validators.required],
      department: this.fb.group({
        id: ['', Validators.required]
      })
    })
  }


  handleSave() {
    const convertedDate = moment(this.projectForm.get('dateStart').value).format("YYYY-MM-DD");
    this.projectForm.get(['department','id']).setValue(this.departmentId)
    this.projectForm.get('dateStart').setValue(convertedDate)
    this.store.dispatch(createProject({createdProject: this.projectForm.value}))
  }
}
