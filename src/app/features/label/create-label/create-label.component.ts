import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import moment from 'moment';
import { loadDepartment } from 'src/app/core/store/department/department.action';
import { selectDepartmentList } from 'src/app/core/store/department/department.selector';
import { createLabel } from 'src/app/core/store/label/label.action';
import { createProject } from 'src/app/core/store/project/project.action';

@Component({
  selector: 'app-create-label',
  templateUrl: './create-label.component.html',
  styleUrls: ['./create-label.component.scss']
})
export class CreateLabelComponent {
  labelForm: FormGroup;
  selectDepartMents$ = this.store.select(selectDepartmentList)
  departmentId: number;
  constructor(
    private fb: FormBuilder,
    private store: Store
  ){}

  ngOnInit() {
    this.store.dispatch(loadDepartment())
    this.labelForm = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
    })
  }


  handleSave() {
    this.store.dispatch(createLabel({createdLabel: this.labelForm.value}))
  }
}
