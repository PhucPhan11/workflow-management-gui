import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { TaskModel } from 'src/app/core/models/task.model';
import { loadTask } from 'src/app/core/store/Task/task.action';
import { selectTasks } from 'src/app/core/store/Task/task.selector';
import { loadLabel } from 'src/app/core/store/label/label.action';
import { selectLabelList } from 'src/app/core/store/label/label.selector';
import { CreateLabelComponent } from './create-label/create-label.component';
import * as labelAction from './../../core/store/label/label.action'

@Component({
  selector: 'app-label',
  templateUrl: './label.component.html',
  styleUrls: ['./label.component.scss']
})
export class LabelComponent implements OnInit{
  @ViewChild(MatPaginator) paginator: MatPaginator;
  labels$: Observable<any> = this.store.select(selectLabelList)
  tasks$: Observable<TaskModel[]> = this.store.select(selectTasks)
  displayedColumns: string[] = ['No','name','description','action'];

  dataSource = new MatTableDataSource<any>();

  constructor(
    private store: Store,
    private dialog: MatDialog,
  ) {
    this.store.dispatch(loadLabel())
    this.store.dispatch(loadTask())
    this.labels$.subscribe(data => {
      this.dataSource.data = data
      // this.dialog.closeAll();
    })
    // this.labels$.subscribe(data => {
    //   this.dataSource.data = data;
    //   this.tasks$.subscribe(task => {
    //     task.map((t:TaskModel) => {
    //       t.label.filter(id => id === data.id)
    //     })
    //   })
    // })

  }
  
  ngOnInit(): void {
    
  }
  openDialog() {
    this.dialog.open(CreateLabelComponent);
  }
  handleDel(id) {
      this.store.dispatch(labelAction.deleteLabel({id}))
  }
}
