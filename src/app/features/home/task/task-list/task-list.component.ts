import {COMMA, ENTER} from '@angular/cdk/keycodes';
import { LiveAnnouncer } from '@angular/cdk/a11y';
import { Component, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Store } from '@ngrx/store';
import { Observable, map, startWith } from 'rxjs';
import { TaskModel } from 'src/app/core/models/task.model';
import { selectTasks } from 'src/app/core/store/Task/task.selector';
import { TaskState } from 'src/app/core/store/Task/task.state';
import * as taskAction from '../../../../core/store/Task/task.action'
import { selectLabelList } from 'src/app/core/store/label/label.selector';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { loadLabel } from 'src/app/core/store/label/label.action';
import { selectDepartmentList } from 'src/app/core/store/department/department.selector';
import { selectProjectList } from 'src/app/core/store/project/project.selector';
import { selectSemesterList } from 'src/app/core/store/semester/semester.selector';
import { loadDepartment } from 'src/app/core/store/department/department.action';
import { loadProject } from 'src/app/core/store/project/project.action';
import { loadSemester } from 'src/app/core/store/semester/semester.action';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent {
  group: FormGroup;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  labelCtrl = new FormControl('')
  selectDepartMents$ = this.store.select(selectDepartmentList)
  selectSemester$ = this.store.select(selectSemesterList)
  selectProject$ = this.store.select(selectProjectList)
  selectLabel$ = this.store.select(selectLabelList)
  tasks$: Observable<TaskModel[]> = this.store.select(selectTasks)
  labels$: Observable<any> = this.store.select(selectLabelList)
  filteredLabels: Observable<any>;
  allLabels: any[]
  taskAdded: any[];
  status: string[] = ["WAITING","TODO","INPROGRESS","TESTING","DONE"]
  priority: string[] = ["HIGHEST","HIGH","MEDIUM","LOW","LOWEST"]
  displayedColumns: string[] =
  ['departmentName', 'description', 'employeeId', 'file'];
  dataSource = new MatTableDataSource<TaskModel>();


  constructor(
    private store: Store,
    private _liveAnnouncer: LiveAnnouncer,
    private fb: FormBuilder
  ) {
    this.store.dispatch(loadLabel())
    this.filteredLabels = this.labelCtrl.valueChanges.pipe(
      startWith(null),
      map((fruit: string | null) => (fruit ? this._filter(fruit) : this.allLabels.slice())),
    )
  }
  ngOnInit() {
    this.group = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      employeeId: [0],
      status: ['', Validators.required],
      priority: ['', Validators.required],
      file: ['',Validators.required],
      semester: this.fb.group({
        id: ['', Validators.required]
      }),
      department: this.fb.group({
        id: ['', Validators.required]
      }),
      project: this.fb.group({
        id: ['', Validators.required]
      }),
      label: this.fb.group({
          id: ['',Validators.required]
      })
    });
    this.store.dispatch(loadDepartment())
    this.store.dispatch(loadSemester())
    this.store.dispatch(loadProject())
    this.store.dispatch(loadLabel())
    this.store.dispatch(taskAction.loadTask())
    this.labels$.subscribe(label => {
      this.allLabels = label
    })
    this.tasks$.subscribe((data:TaskModel[]) => {
      this.dataSource.data = data
      
      // this.taskAdded = data['label']
      console.log(this.taskAdded)
    })
   
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator
  }
  add(event){
    const value = (event.value || '').trim();

    // Add our fruit
    if (value) {
      // this.store.dispatch(create)
    }

    // Clear the input value
    event.chipInput!.clear();

  }
  remove(event) {

  }
  selected(event) {

  }

  private _filter(value: string): string[] { 
    const filterValue = value.toLowerCase();
    return this.allLabels.filter(fruit => fruit.name.toLowerCase().includes(filterValue));
  }
}
