import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CreateTaskComponent } from './create-task/create-task.component';
import { Store } from '@ngrx/store';
import { selectLabelList } from 'src/app/core/store/label/label.selector';
import { Observable } from 'rxjs';
import { selectSemesterList } from 'src/app/core/store/semester/semester.selector';
import { loadLabel } from 'src/app/core/store/label/label.action';
import { loadSemester } from 'src/app/core/store/semester/semester.action';
import { loadDepartment } from 'src/app/core/store/department/department.action';
import { loadProject } from 'src/app/core/store/project/project.action';

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss']
})
export class TaskComponent implements OnInit {
  label$: Observable<any> = this.store.select(selectLabelList)
  semester$: Observable<any> = this.store.select(selectSemesterList)
  constructor(
    public dialog: MatDialog,
    public store: Store) {}
  ngOnInit(): void {
    this.store.dispatch(loadSemester())
    this.store.dispatch(loadDepartment())
    this.store.dispatch(loadProject())
  }

  openDialog() {
    this.dialog.open(CreateTaskComponent);
  }
}
