import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import moment from 'moment';
import { createTask } from 'src/app/core/store/Task/task.action';
import { loadDepartment } from 'src/app/core/store/department/department.action';
import { selectDepartmentList } from 'src/app/core/store/department/department.selector';
import { loadLabel } from 'src/app/core/store/label/label.action';
import { selectLabelList } from 'src/app/core/store/label/label.selector';
import { createProject, loadProject } from 'src/app/core/store/project/project.action';
import { selectProjectList } from 'src/app/core/store/project/project.selector';
import { loadSemester } from 'src/app/core/store/semester/semester.action';
import { selectSemesterList } from 'src/app/core/store/semester/semester.selector';

@Component({
  selector: 'app-create-task',
  templateUrl: './create-task.component.html',
  styleUrls: ['./create-task.component.scss']
})
export class CreateTaskComponent {
  name: string
  projectForm: FormGroup;
  selectDepartMents$ = this.store.select(selectDepartmentList)
  selectSemester$ = this.store.select(selectSemesterList)
  selectProject$ = this.store.select(selectProjectList)
  selectLabel$ = this.store.select(selectLabelList)
  statusId: number;
  priorityId: number;
  semesterId: number;
  projectId: number;
  labelId: number;
  departmentId: number;
  status: string[] = ["WAITING","TODO","INPROGRESS","TESTING","DONE"]
  priority: string[] = ["HIGHEST","HIGH","MEDIUM","LOW","LOWEST"]
  constructor(
    private fb: FormBuilder,
    private store: Store
  ){}

  ngOnInit() {
    this.store.dispatch(loadDepartment())
    this.store.dispatch(loadSemester())
    this.store.dispatch(loadProject())
    this.store.dispatch(loadLabel())
    this.projectForm = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
      employeeId: [0],
      status: ['', Validators.required],
      priority: ['', Validators.required],
      file: ['',Validators.required],
      semester: this.fb.group({
        id: ['', Validators.required]
      }),
      department: this.fb.group({
        id: ['', Validators.required]
      }),
      project: this.fb.group({
        id: ['', Validators.required]
      }),
      label: this.fb.group({
          id: ['',Validators.required]
      })
    })
  }


  handleSave() {
    this.projectForm.get(['semester','id']).setValue(this.semesterId)
    this.projectForm.get(['department','id']).setValue(this.departmentId)
    this.projectForm.get(['project','id']).setValue(this.projectId)
    this.projectForm.get(['label','id']).setValue(this.labelId)
    this.projectForm.get('status').setValue(this.statusId)
    this.projectForm.get('priority').setValue(this.priorityId)
    console.log(this.projectForm.value)
    this.store.dispatch(createTask({createdTask: this.projectForm.value}))
  }
}
