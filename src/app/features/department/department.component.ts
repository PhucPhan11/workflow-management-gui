import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { TaskModel } from 'src/app/core/models/task.model';
import { loadTask } from 'src/app/core/store/Task/task.action';
import { selectTasks } from 'src/app/core/store/Task/task.selector';
import { loadLabel } from 'src/app/core/store/label/label.action';
import { selectLabelList } from 'src/app/core/store/label/label.selector';
import { CreateLabelComponent } from '../label/create-label/create-label.component';
import { createDepartment, loadDepartment } from 'src/app/core/store/department/department.action';
import { selectDepartmentList } from 'src/app/core/store/department/department.selector';
import { CreateDepartmentComponent } from './create-department/create-department.component';

@Component({
  selector: 'app-department',
  templateUrl: './department.component.html',
  styleUrls: ['./department.component.scss']
})
export class DepartmentComponent {
  @ViewChild(MatPaginator) paginator: MatPaginator;
  department$: Observable<any> = this.store.select(selectDepartmentList)
  tasks$: Observable<TaskModel[]> = this.store.select(selectTasks)
  displayedColumns: string[] = ['No','name','description','action'];

  dataSource = new MatTableDataSource<any>();

  constructor(
    private store: Store,
    private dialog: MatDialog,
  ) {
    this.store.dispatch(loadDepartment())
    this.store.dispatch(loadTask())
    this.department$.subscribe(data => {
      this.dataSource.data = data
    })
    // this.labels$.subscribe(data => {
    //   this.dataSource.data = data;
    //   this.tasks$.subscribe(task => {
    //     task.map((t:TaskModel) => {
    //       t.label.filter(id => id === data.id)
    //     })
    //   })
    // })

  }
  
  ngOnInit(): void {
    
  }
  openDialog() {
    this.dialog.open(CreateDepartmentComponent);
  }
  handleDel(id) {
      // this.store.dispatch(.deleteLabel({id}))
  }
}
