import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Store } from '@ngrx/store';
import { loadDepartment } from 'src/app/core/store/department/department.action';
import { selectDepartmentList } from 'src/app/core/store/department/department.selector';
import { createLabel } from 'src/app/core/store/label/label.action';

@Component({
  selector: 'app-create-department',
  templateUrl: './create-department.component.html',
  styleUrls: ['./create-department.component.scss']
})
export class CreateDepartmentComponent {
  labelForm: FormGroup;
  departmentId: number;
  constructor(
    private fb: FormBuilder,
    private store: Store
  ){}

  ngOnInit() {
    this.labelForm = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
    })
  }


  handleSave() {
    this.store.dispatch(createLabel({createdLabel: this.labelForm.value}))
  }
}
